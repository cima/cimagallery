<?php
namespace cimagallery;

require_once("PluginSettings.php");

class VirtualPage {

    public const GALLERY_DIR_QUERY_VAR = 'cimagallery';
    
    public function __construct() {}
    
    public static function registerRewrites() {
        $pluginSettings = new PluginSettings();
        
        add_rewrite_rule(
            "^" . $pluginSettings->getPermalinkPrefix() . "/(.+?)/?$", 
            'index.php?' . self::GALLERY_DIR_QUERY_VAR . '=$matches[1]', 
            "top"
        );
        
        add_rewrite_rule(
            "^" . $pluginSettings->getPermalinkPrefix() . "/?$",
            'index.php?' . self::GALLERY_DIR_QUERY_VAR . '=.',
            "top"
        );
    }
    
    public static function registerQueryVars($vars) {
        $vars[] = self::GALLERY_DIR_QUERY_VAR;
        return $vars;
    }
    
    public static function templateMatcher($template) {
        global $wp_query;
        
        $new_template = '';
        
        if (array_key_exists(self::GALLERY_DIR_QUERY_VAR, $wp_query->query_vars)) {

            $new_template = realpath(dirname(__FILE__) . "/template-cimagallery.php");
            
            if ($new_template != '') {
                return $new_template;
            } else {
                // This is not a valid virtualpage value, so set the header and template for a 404 page.
                $wp_query->set_404();
                status_header(404);
                return get_404_template();
            }
        }
        
        return $template;
    }
}

