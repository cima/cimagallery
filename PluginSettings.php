<?php
namespace cimagallery;

class PluginSettings {
    
    private const OPTION_GROUP = 'cimagallery.settings';
    private const GALLERY_ROOT = 'cimagallery_galleryRootLocation';
    private const PERMALINK_PREFIX = 'cimagallery_galleryPermalinkPrefix';
    private const THUMBNAIL_WIDTH_LIMIT = 'cimagallery_thumbnailWidthLimit';
    private const THUMBNAIL_HEIGHT_LIMIT = 'cimagallery_thumbnailHeightLimit';
    
    public function __construct__() {
        //nothing for now
    }
    
    /**
     * Register plugin settings variables to wordpress infrastructure and database.
     * */
    public static function registerVariables() {
        add_option(self::GALLERY_ROOT, './');
        add_option(self::PERMALINK_PREFIX, 'gallery');
        add_option(self::THUMBNAIL_WIDTH_LIMIT, '300');
        add_option(self::THUMBNAIL_HEIGHT_LIMIT, '115');
        
        register_setting( self::OPTION_GROUP, self::GALLERY_ROOT, 'myplugin_callback' );
        register_setting( self::OPTION_GROUP, self::PERMALINK_PREFIX, 'myplugin_callback' );
        register_setting( self::OPTION_GROUP, self::THUMBNAIL_WIDTH_LIMIT, 'myplugin_callback' );
        register_setting( self::OPTION_GROUP, self::THUMBNAIL_HEIGHT_LIMIT, 'myplugin_callback' );
    }
    
    public function getGalleryRoot() {
        return get_option(self::GALLERY_ROOT);
    }
    
    public function getPermalinkPrefix() {
        return get_option(self::PERMALINK_PREFIX);
    }
        
    /**
     * Register user friendly page to change plugin global settings.
     * */
    public static function registerOptionsPage() {
        add_options_page(
            'Cimagallery settings', 
            'Cima Gallery', 
            'manage_options', //cpabilities - which rights/priviledges user must have to see this page 
            'cimagallery', // menu slug
            __CLASS__ . '::optionsPage' //reference to method that generates the page
        );
    }
    
    /**
     * Generate the page with plugin's global settings.
     * */
    public static function optionsPage(){
    ?>
      <div>
      <?php screen_icon(); ?>
      <h2>Cima gallery settings</h2>
      
      <form method="post" action="options.php">
          <?php echo settings_fields(self::OPTION_GROUP); ?>
          <h3>Gallery directories</h3>
          
          <table>
          
              <tr valign="top">
                  <th scope="row"><label for="<?php echo self::GALLERY_ROOT ?>">Galleries root directory</label></th>
                  <td>
                   	<input type="text" id="<?php echo  self::GALLERY_ROOT ?>" name="<?php echo  self::GALLERY_ROOT ?>" value="<?php echo  get_option(self::GALLERY_ROOT); ?>" /><br/>
                   	Where are the galeries located relatively to the root of the web. This folder (and its subfolders) must have publicly accessible files, because images will be served directly from here.
                  </td>
              </tr>
                
                
              <tr valign="top">
                  <th scope="row"><label for="<?php echo self::PERMALINK_PREFIX ?>">Gallery permalink path prefix</label></th>
                  <td>
                  	  <input type="text" id="<?php echo self::PERMALINK_PREFIX ?>" name="<?php echo self::PERMALINK_PREFIX ?>" value="<?php echo get_option(self::PERMALINK_PREFIX); ?>" /><br/>
                  	  The word that will trigger gallery rendering when present as a first path item in URL.<br/>
                  	  Due to this plugin beta nature this word mustn't colide with above root folder.
                  </td>
              </tr>
                
              <tr valign="top">
                  <th scope="row"><label for="<?php echo self::THUMBNAIL_WIDTH_LIMIT ?>">Thumbnail width limit [px]</label></th>
                  <td><input type="text" id="<?php echo self::THUMBNAIL_WIDTH_LIMIT ?>" name="<?php echo self::THUMBNAIL_WIDTH_LIMIT ?>" value="<?php echo get_option(self::THUMBNAIL_WIDTH_LIMIT); ?>" /></td>
              </tr>
              		
              <tr valign="top">
                  <th scope="row"><label for="<?php echo self::THUMBNAIL_HEIGHT_LIMIT ?>">Thumbnail height limit [px]</label></th>
                  <td><input type="text" id="<?php echo self::THUMBNAIL_HEIGHT_LIMIT ?>" name="<?php echo self::THUMBNAIL_HEIGHT_LIMIT ?>" value="<?php echo get_option(self::THUMBNAIL_HEIGHT_LIMIT); ?>" /></td>
              </tr>
          		
          </table>
            
          <?php  submit_button(); ?>
      </form>
      </div>
    <?php
    } 
}

?>