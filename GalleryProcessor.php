<?php
namespace cimagallery;

use cimagallery\model\GalleryDescriptionBuilder;
use cimagallery\model\ImageItem;
use Imagick;

require_once("model/GalleryDescriptionBuilder.php");
require_once("model/ImageItem.php");

class GalleryProcessor
{
    
    private const DESCRIPTION_FILE = "info.xml";
    
    private $galleryLocation = false;
    private $galleryUriLocation = false;
    
    private $items = array();
    
    private $widthLimit = 300;
    private $heightLimit = 115;
    
    /**
     * @param $galleryLocation string relative path to files in filesystem
     * @param $galleryUriLocation string URI path to galery
     * */
    public function __construct($galleryLocation, $galleryUriLocation = false) {
        $this->galleryLocation = $galleryLocation;
        $this->galleryUriLocation = $galleryUriLocation ? $galleryUriLocation : $galleryLocation;
    }
    
    /**
     * Gathers information about gallery represented by this object. 
     * 
     * $builder GalleryDescriptionBuilder into which to fill details about gallery represented by this object
     * */
    public function getGalleryInfo(GalleryDescriptionBuilder $builder){
        $galleryDir = $this->galleryLocation;
        $descr = realpath( $galleryDir . "/" . self::DESCRIPTION_FILE);
        $description = false;
        
        $builder->setLocation($galleryDir);
        $builder->setUriPath($this->galleryUriLocation);
        
        if (file_exists($descr)) {
            $description = simplexml_load_file($descr);
        }
        
        if($description !== false){
            $builder->setName($description->name);
            
            if(isset($description->date)){
                $builder->setDate(strtotime($description->date));
            }
            
            if(isset($description->endDate)){
                $builder->setEndDate(strtotime($description->endDate));
            }
            
            if(isset($description->description)){
                $builder->setDescription($description->description);
            }
            
            if(isset($description->thumbnail)){
                $builder->setThumbnail(array(ImageItem::create($this->galleryLocation, $description->thumbnail)));
            }
        }
        
        if($builder->name === false){
            $builder->setName(basename(realpath($galleryDir)));
        }
        
        if($builder->date === false){
            $builder->setDate(filemtime($galleryDir));
        }
        
        if($builder->thumbnail === false) {
            $builder->setThumbnail($this->listGalleryFiles());
        }
    }
    
    /**
     * Return list of all images in gallery, ordered lexicographically.
     * */
    public function listGalleryFiles() {
        $items = array();
        
        if (isset($this->galleryLocation)){
            
            $dirHandle = opendir($this->galleryLocation);
            
            while($item = readdir($dirHandle)){
                if($item[0] == '.'){
                    continue; // skip hidden, self and parent
                }
                
                if(is_dir($this->galleryLocation . "/" . $item)){
                    continue;
                }
                
                if(preg_match("/.*\\.(jpe?g|png|gif)/i", $item)){
                    $img = ImageItem::create($this->galleryLocation, $item);
                    $items[] = $img;
                    $this->initThumbnail($img);
                }                
            }
        }
        
        sort($items);
        return $items;
    }
    
    /**
     * Return available subgalleries for gallery represented by this object.
     * @return 
     * */
    public function listSubgalleries() {
        $subgalleries = array();
        
        if (isset($this->galleryLocation)){
            
            $dirHandle = opendir($this->galleryLocation);
            
            while($item = readdir($dirHandle)){
                
                if($item[0] == '.'){
                    continue; // skip hidden, self and parent
                }
                
                if( ! is_dir($this->galleryLocation . "/" . $item) || $item == model\ImageItem::THUMBNAIL_DIR){
                    continue;
                }
                
                $descr = new model\GalleryDescription();
                $subProcessor = new GalleryProcessor($this->galleryLocation . "/" . $item, $this->galleryUriLocation . "/" . $item);
                $subProcessor->getGalleryInfo($descr);
                $subgalleries[] = $descr;
            }
        }

        usort($subgalleries, array(__CLASS__, "sortByDate"));
        return $subgalleries;
    }
    
    /**
     * Sort galleries by date in descendatn order (newer galleries first)
     * 
     * @param $a
     * @param $b
     * @return +x if $a has an older date than $b
     *          0 if $a has a same date as $b
     *         -x if $a has a newer date than $b
     * */
    public static function sortByDate($a, $b) {
        return $b->date - $a->date;
    }
    
    // ---------------- thumbnail generation --------------------
    private function initThumbnail(ImageItem $img) {
        
        $galleryLocation = $this->galleryLocation;
        
        if(file_exists($img->getFullThumbnail()) ) {
            return;
        }
        
        $this->generateThumbnail($img, $this->widthLimit, $this->heightLimit);
    }
    
    private function getSmallDir(ImageItem $img) {
        return dirname($img) . "/small/";
    }
    
    private function initSmallDir(ImageItem $img) {
        $smallDir = dirname($img->getFullThumbnail());
        if( ! is_dir($smallDir)) {
            mkdir($smallDir);
        }
    }
    
    private function generateThumbnail(ImageItem $img, $width, $height, $quality = 90)
    {
        if (is_file($img->getFullFilename())) {
            
            $this->initSmallDir($img);
            
            $imagick = new Imagick(realpath($img->getFullFilename()));
            $imagick->setImageFormat('jpeg');
            $imagick->setImageCompression(Imagick::COMPRESSION_JPEG);
            $imagick->setImageCompressionQuality($quality);
            
            $imagick->thumbnailImage($width, $height, true, false);
            
            if (file_put_contents($img->getFullThumbnail(), $imagick) === false) {
                echo "Could not put contents.";
                //throw new Exception("Could not put contents.");
            }
            return true;
        }
        else {
            echo "No valid image provided with " . $img->getFullFilename() . ".";
            //throw new Exception("No valid image provided with {$img}.");
        }
    }
    
    
    // ----------- getters / setters ------------
    public function setThumbnailWidthLimit($maxWidthPx) {
        $this->widthLimit = $maxWidthPx;
    }
    
    public function setThumbnailHeightLimit($maxHeightPx) {
        $this->HeightLimit = $maxHeightPx;
    }
}

?>
