<?php
namespace cimagallery;

class Design
{

    const LIGHT_BOX_HANDLE = "cima_lighbox";
    const CIMA_STYLES = "cima_gallery_styles";
    
    public function __construct(){}
    
    public static function registerStylesAndScripts() {
        wp_register_style(self::CIMA_STYLES, plugins_url('css/cimagallery.css', __FILE__ ));
        
        wp_register_style(self::LIGHT_BOX_HANDLE, plugins_url('css/lightbox.css',__FILE__ ));
        wp_register_script(self::LIGHT_BOX_HANDLE, plugins_url('js/lightbox.js',__FILE__ ), array("jquery"));
    }
    
    function enqueueStylesAndScripts(){
        wp_enqueue_style(self::CIMA_STYLES, false, NULL, "all");
        
        wp_enqueue_style(self::LIGHT_BOX_HANDLE, false, NULL, "all");
        wp_enqueue_script(self::LIGHT_BOX_HANDLE);
    }
}

