<?php
if( ! class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class CImaGalleryListTable extends WP_List_Table {
	
	function __construct() {
       parent::__construct( array(
      'singular'=> 'wp_list_text_link', //Singular label
      'plural' => 'wp_list_test_links', //plural label, also this well be one of the table css class
      'ajax'   => false //We won't support Ajax for this table
      ) );
    }
    
   function get_columns() {
	return $columns =
		array(
    		'col_link_id'=>'ID',
			'col_link_name'=>'Name'
		);
   }
   
   function get_sortable_columns() {
	return $columns =
		array(
    		'col_link_id' => array('cima_id', false),
			'col_link_name' => array('cima_name', false)
		);
   }
   		
   function prepare_items() {

   	  $this->_column_headers = array(
   	  	$this->get_columns(),
   	  	array(),
   	  	$this->get_sortable_columns(),
   	  	"col_link_id"
   	  );
   	  
   	  $this->items = array(
   	  		array("col_link_id" => "1", "col_link_name" => "gizmo")
   	  );
   }
   
   function column_col_link_id($item){
   	   return $item["col_link_id"];	   
   }
   
   function column_col_link_name($item){
   	   return $item["col_link_name"];
   }
}

?>