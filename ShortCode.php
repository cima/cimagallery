<?php
namespace cimagallery;

require_once("Design.php");
require_once("PluginSettings.php");
require_once("GalleryProcessor.php");

class ShortCode
{
    public const SHORT_CODE = "cimagallery";
    public const GALLERY_DIR_ATTR = "gallerydir";
    
    public function __construct(){}
    
    public static function renderGallery($atts) {
        
        $params = shortcode_atts(array(
            self::GALLERY_DIR_ATTR => ".",
        ), $atts);
        
        $design = new Design();
        $design->enqueueStylesAndScripts();
        
        $pluginSettings = new PluginSettings();
        $webAbsoluteGalleryDir = $pluginSettings->getGalleryRoot() . "/" . $params[self::GALLERY_DIR_ATTR];
        
        $galleryProcessor = new GalleryProcessor($webAbsoluteGalleryDir);
        $items = $galleryProcessor->listGalleryFiles();

        foreach($items as $img) {
            ?>
			<a href="/<?php echo $img->getFullFilename() ?>" data-lightbox="image-cima" title="">
				<img src="/<?php echo $img->getFullThumbnail() ?>" style="height: 115px;" />
			</a>
    		<?php
    	}

    }

}