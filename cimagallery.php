<?php
/**
 * @package CimaGallery
 * @version 1.0.0
 */
/*
Plugin Name: Cima's gallery
Plugin URI: https://cima.moxo.cz
Description: Simple file based gallery
Version: 1.0.0
Author: Cima
Author URI: https://cima.moxo.cz
*/

namespace cimagallery;

require_once("PluginSettings.php");
require_once("VirtualPage.php");
require_once("Design.php");
require_once("ShortCode.php");
require_once("BackOffice.php");

// --------------------- [begin] shortcode (slug) -----------------------------------------------
add_shortcode(ShortCode::SHORT_CODE, __NAMESPACE__ . "\\ShortCode::renderGallery");
// --------------------- [end] shortcode (slug) -----------------------------------------------

// --------------------- [begin] rewrite ------------------
add_action('init',  __NAMESPACE__ . "\\VirtualPage::registerRewrites", 100, 0);
add_filter('query_vars', __NAMESPACE__ . "\\VirtualPage::registerQueryVars");
add_filter('template_include',  __NAMESPACE__ . "\\VirtualPage::templateMatcher");
// --------------------- [begin] rewrite ------------------

// --------------------- [begin] JS and Styles -----------------------------------------------
add_action( 'init',  __NAMESPACE__ . "\\Design::registerStylesAndScripts");
// --------------------- [end] JS and Styles -----------------------------------------------

// --------------------- [begin] configuration & settings pages -----------------------------------------------
add_action('admin_init', __NAMESPACE__ . "\\PluginSettings::registerVariables");
add_action('admin_menu', __NAMESPACE__ . "\\PluginSettings::registerOptionsPage");
add_action('admin_menu', __NAMESPACE__ . "\\BackOffice::registerAdminpage");
// --------------------- [end] configuration & settings pages -----------------------------------------------

?>
