<?php
/**
 * The main template file for cimagallery.
 * Shows images in gallery based on folder content and nfo.xml file in given folder
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */
namespace cimagallery;

require_once("Design.php");
require_once("model/GalleryDescription.php");
require_once("PluginSettings.php");
require_once("VirtualPage.php");

$pluginSettings = new PluginSettings();

function getCandyOrder($galleryDescription, $order) {
    return ((crc32($galleryDescription->name) >> ($order % 4)) & 0xff) % 12 + 1;
}

function getTile($galleryDescription, $order) {
    $small = $galleryDescription->thumbnail;
    $color = getCandyOrder($galleryDescription, $order);
    $imageIndex = intval(($order % 4) * (sizeof($small) / 4.0));
    ?>
    <div class="cimagallery_tile cimagallery_<?php echo $order ?> Candy<?php echo $color ?>">
    <?php if(array_key_exists($order, $small)) { ?>
    	<img src="/<?php echo $small[$imageIndex]->getFullThumbnail() ?>"/>
    <?php } ?>
    </div>
    <?php
}

get_header(); ?>
	
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			$design = new Design();
			$design->enqueueStylesAndScripts();
					
			$root = $pluginSettings->getGalleryRoot();
			$uriPrefix = $pluginSettings->getPermalinkPrefix();
			$specificGallery =  "/" . $wp_query->query_vars[VirtualPage::GALLERY_DIR_QUERY_VAR];
			
			$galleryProcessor = new GalleryProcessor($root . $specificGallery, $uriPrefix . $specificGallery);
			
			// ---------- metadata -------------
			$descr = new model\GalleryDescription();
			$galleryProcessor->getGalleryInfo($descr);

			echo "<h1>" . $descr->name . "</h1> <a href=\"..\" class=\"cimagallery_back_button Candy" . getCandyOrder($descr, 1) . "\"></a> <i class=\"Date\">" . date("j. F Y", $descr->date) . "</i>";
			
			/*
			if (isset($descr->thumbnail)){
				echo "<a href=\"$dirEntry\"><img src=\"" . $dirEntry . "/small/" . $descr->thumbnail . "\" alt=\"\" class=\"Thumbnail\"></a>";
			}
			*/
			
			if (isset($descr->description)){
				echo "<p>" . $descr->description . "</p>";
			}
			// ---------- metadata -------------
			
			// ---------- subgalleries -------------
			?>
			
			<div class="cimagallery_subgalleries">
			
			<?php
			$subgalleries = $galleryProcessor->listSubgalleries();
			foreach($subgalleries as $galleryDescription) {
			    ?>

				
				<a href="/<?php echo $galleryDescription->uriPath ?>" title="<?php echo $galleryDescription->date ?>">
				<div class="cimagallery_quad">
					
            		<div class="cimagallery_caption"><?php echo $galleryDescription->name ?></div>
            		
            		<?php getTile($galleryDescription, 1) ?>
            		<?php getTile($galleryDescription, 2) ?>
            		<?php getTile($galleryDescription, 3) ?>
            		<?php getTile($galleryDescription, 4) ?>

            	</div></a>
				
			    <?php
			}
			
			?>
			
			</div>
			
			<?php
			// ---------- subgalleries -------------
			
			// ---------- individual photos ----
			?>
			
			<div class="cimagallery_photogallery">
			
			<?php

			$items = $galleryProcessor->listGalleryFiles();
			foreach($items as $img) {
				?>
					<a href="/<?php echo $img->getFullFilename() ?>" data-lightbox="image-cima" title="">
						<img src="/<?php echo $img->getFullThumbnail() ?>" style="height: 115px;" />
					</a>
				<?php
			}
			?>
			
			</div>
			
			<?php
			// ---------- individual photos ----
							
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
