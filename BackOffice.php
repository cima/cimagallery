<?php
namespace cimagallery;

require_once("CimaGalleryListTable.php");

class BackOffice
{

    public function __construct() {}
    
    public static function registerAdminPage() {
        add_menu_page(
            "Cima Gallery",
            "Cima Gallery",
            "manage_options", //TODO broader capability like edit/create posts
            "cimagallerymenu",
            __CLASS__ . "::renderAdminPage",
            "" //plugin_dir_url(__FILE__) . "images/gizmo-images.png"
            );
    }
    
    public static function renderAdminPage() {
        
        $galleryTable = new \CImaGalleryListTable(); //TODO namespace
        $galleryTable->prepare_items();
        $galleryTable->display();
        
    ?>

    <?php
    }
}

