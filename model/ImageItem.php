<?php
namespace cimagallery\model;

class ImageItem {
    
    public const THUMBNAIL_DIR = "small"; 
    
    public $filename = false;
    public $thumbnail = false;
    
    public $base = false;
    
    public function __construct($base, $filename, $thumbnail) {
        $this->base = $base;
        $this->filename = $filename;
        $this->thumbnail = $thumbnail;
    }
    
    public function getFullFilename() {
        return $this->base . "/" . $this->filename;
    }
    
    public function getFullThumbnail() {
        return $this->base . "/" . $this->thumbnail;
    }
    
    /**
     * Create item with default thumbnail location at ./small/$filename
     * */
    public static function create($base, $filename) {
        return new ImageItem($base, $filename, self::THUMBNAIL_DIR . "/" . $filename);
    }
}

