<?php
namespace cimagallery\model;

require_once("GalleryDescriptionBuilder.php");

class GalleryDescription implements GalleryDescriptionBuilder {
    public $name = false;
    public function setName($nameArg){
        $this->name = $nameArg;
    }
    
    public $date = false;
    public function setDate($dateArg){
        $this->date = $dateArg;
    }
    
    public $endDate = false;
    public function setEndDate($endDateArg){
        $this->endDate = $endDateArg;
    }
    
    public $description = false;
    function setDescription($descriptionArg){
        $this->description = $descriptionArg;
    }
    
    public $thumbnail = false;
    function setThumbnail($thumbnailArg){
        $this->thumbnail = $thumbnailArg;
    }
    
    public $location = false;
    function setLocation($locationArg){
        $this->location = $locationArg;
    }
    
    public $uriPath = false;
    function setUriPath($uriPathArg){
        $this->uriPath = $uriPathArg;
    }
}

?>
