<?php
namespace cimagallery\model;

interface GalleryDescriptionBuilder {
    function setName($name);
    function setDate($date);
    function setEndDate($endDate);
    function setDescription($description);
    function setThumbnail($thumb);
    function setLocation($location);
    function setUriPath($uriPath);
}

?>
